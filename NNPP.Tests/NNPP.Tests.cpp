// NNPP.Tests.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <chrono>
#include "../NNPP/inc/Matrix.h"
#include "..//NNPP/inc/activations/SoftMax.h"

using namespace NNPP;

int main()
{
	const int side = 1000;
	/*Matrix a(side, side, true);
	Matrix b(side, side, true);

	Matrix c(side, side, true);

	for (int i = 0; i < side; i++)
	{
		float f[side];
		float g[side];
		for (int j = 0; j < side; j++)
		{
			f[j] = j * (i + 1);
			g[j] = (j + 1) * (i + 1);
		}
		a.write(f, i * side, side);
		b.write(g, i * side, side);
	}
	long long final_Val = 0;

	for (int j = 0; j < 100; j++) {
		auto start = std::chrono::high_resolution_clock::now();

		Matrix::mad(&a, &b, nullptr, nullptr, &c, false);

		auto finish = std::chrono::high_resolution_clock::now();
		auto diff = std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start).count();
		final_Val += diff;
	}
	final_Val /= 100;
	final_Val /= 1000000;
	std::cout << final_Val << "ms\n";
	*/

	SoftMax smax;
	auto v = smax.getActivation();

	float g[3];
	float h[3];

	g[0] = 2.0f;
	g[1] = 1.0f;
	g[2] = 0.1f;
	
	for(int i = 0; i < 3; i++)
		std::cout << v->CPUFunction(g, i, 3) << "\n";

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
