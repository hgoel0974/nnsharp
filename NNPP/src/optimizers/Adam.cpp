#include "optimizers/Adam.h"
#include "ThreadPool.h"

using namespace NNPP;

Adam::Adam(int layer_cnt, float learning_rate, float epsilon, float beta_1, float beta_2) {
	this->layers = new AdamParams[layer_cnt]();
	this->learning_rate = learning_rate;
	this->beta_1 = beta_1;
	this->beta_2 = beta_2;
	this->epsilon = epsilon;
	this->layer_cnt = layer_cnt;
	Net = 1;
}

Adam::~Adam() {
	for (int i = 0; i < layer_cnt; i++) {
		delete[] layers[i].m_w;
		delete[] layers[i].v_w;
		delete[] layers[i].m_b;
		delete[] layers[i].v_b;
	}
	delete[] layers;
}

int Adam::RegisterLayer(ILayer* layer, int w_cnt, int ww_len, int wh_len, int b_cnt, int b_len)
{
	if (idx >= layer_cnt)
		throw "Too many layers";

	layers[idx].m_w = new Matrix[w_cnt];
	layers[idx].v_w = new Matrix[w_cnt];
	layers[idx].m_b = new Matrix[b_cnt];
	layers[idx].v_b = new Matrix[b_cnt];

	for (int i = 0; i < w_cnt; i++)
	{
		layers[idx].m_w[i].init(ww_len, wh_len, true);
		layers[idx].v_w[i].init(ww_len, wh_len, true);
	}

	for (int i = 0; i < b_cnt; i++)
	{
		layers[idx].m_b[i].init(b_len, 1, true);
		layers[idx].v_b[i].init(b_len, 1, true);
	}

	idx++;
}

void Adam::OptimizeWeights(ILayer* layer, int l_idx, int idx, Matrix* w, Matrix* nabla_w)
{
	auto func = [this, l_idx, idx, w, nabla_w](int i)
	{
		layers[l_idx].m_w[idx].getMemory()[i] = beta_1 * layers[l_idx].m_w[idx].getMemory()[i] + (1 - beta_1) * nabla_w->getMemory()[i];
		layers[l_idx].v_w[idx].getMemory()[i] = beta_2 * layers[l_idx].v_w[idx].getMemory()[i] + (1 - beta_2) * nabla_w->getMemory()[i] * nabla_w->getMemory()[i];

		L1Val += fabsf(w->getMemory()[i]);
		L2Val += w->getMemory()[i] * w->getMemory()[i];
		Net++;

		w->getMemory()[i] -= (float)(learning_rate / (sqrtf(layers[l_idx].v_w[idx].getMemory()[i] / (1 - beta_2)) + epsilon)) * (layers[l_idx].m_w[idx].getMemory()[i] / (1 - beta_1));
	};
	ThreadPool::parallel_for(0, layers[l_idx].m_w[idx].getLen(), func);
}

void Adam::OptimizeBiases(ILayer* layer, int l_idx, int idx, Matrix* b, Matrix* nabla_b)
{
	auto func = [this, l_idx, idx, b, nabla_b](int i)
	{
		layers[l_idx].m_b[idx].getMemory()[i] = beta_1 * layers[l_idx].m_b[idx].getMemory()[i] + (1 - beta_1) * nabla_b->getMemory()[i];
		layers[l_idx].v_b[idx].getMemory()[i] = beta_2 * layers[l_idx].v_b[idx].getMemory()[i] + (1 - beta_2) * nabla_b->getMemory()[i] * nabla_b->getMemory()[i];

		b->getMemory()[i] -= (float)(learning_rate / (sqrtf(layers[l_idx].v_b[idx].getMemory()[i] / (1 - beta_2)) + epsilon)) * (layers[l_idx].m_b[idx].getMemory()[i] / (1 - beta_1));
	};
	ThreadPool::parallel_for(0, layers[l_idx].m_b[idx].getLen(), func);
}

void Adam::Update(float curError)
{
	L1Val = 0;
	L2Val = 0;
	Net = 0;
}
