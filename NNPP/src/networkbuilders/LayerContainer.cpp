#include "networkbuilders/LayerContainer.h"
#include "IWeightInitializable.h"

using namespace NNPP;

LayerContainer::LayerContainer(int input_side, int input_depth) : layers() {
	this->input_side = input_side;
	this->input_depth = input_depth;
}

LayerContainer* LayerContainer::Append(ILayer* layer) {
	layers.push_back(layer);
}

LayerContainer* LayerContainer::SetLoss(ILossFunction* lossFunc) {
	this->loss_func = lossFunc;
}

LayerContainer* LayerContainer::SetWeightInitializer(IWeightInitializer* init) {
	this->weight_init = init;
}

Matrix** LayerContainer::Forward(Matrix** input) {
	Matrix** p = input;
	for (int i = 0; i < layers.size(); i++) {
		auto l = layers.at(i);
		p = l->forward(p);
	}
	return p;
}

Matrix** LayerContainer::Propagate(Matrix** prev_delta) {
	Matrix** p = prev_delta;
	for (int i = layers.size() - 1; i >= 0; i--) {
		auto l = layers.at(i);
		p = l->propagate(p);
	}
	return p;
}

void LayerContainer::ComputeErrors(Matrix** prev_delta) {
	Matrix** p = prev_delta;
	for (int i = layers.size() - 1; i >= 0; i--) {
		auto l = layers.at(i);
		l->layerError(p);
		p = l->getLastDelta();
	}
}

void LayerContainer::Learn(IOptimizer& opt) {
	for (int i = layers.size() - 1; i >= 0; i--) {
		auto l = layers.at(i);
		l->learn(opt);
	}
}

void LayerContainer::Build() {
	//Propogate the output sizes
	int iside = input_side, idepth = input_depth;
	for (int i = 0; i < layers.size(); i++) {
		auto l = layers.at(i);
		l->setInputSz(iside, idepth);
		iside = l->getOutputSz();
		idepth = l->getOutputDepth();

		auto weightsetup = dynamic_cast<IWeightInitializable*>(l);
		if (weightsetup)
			weightsetup->setWeights(weight_init);
	}

	output_side = iside;
	output_depth = idepth;
}