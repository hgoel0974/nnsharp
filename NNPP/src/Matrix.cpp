#include "Matrix.h"
#include "ThreadPool.h"

using namespace NNPP;

Matrix::Matrix(int col, int row, bool zero) {
	init(col, row, zero);
}

Matrix::Matrix(float* mem, int len, int col, int row, int colstride, int rowstride) {
	this->memory = mem;
	this->len = len;
	this->col = col;
	this->row = row;
	this->colstride = colstride;
	this->rowstride = rowstride;
}

Matrix::Matrix() { }

Matrix::~Matrix() {
	delete transposed;
	delete[] memory;
}

void Matrix::init(int col, int row, bool zero) {
	this->col = col;
	this->row = row;
	this->colstride = 1;
	this->rowstride = col;
	this->len = col * row;

	if (zero)
		this->memory = new float[len] { 0 };
	else
		this->memory = new float[len];
}

int Matrix::getColumns() const {
	return this->col;
}

int Matrix::getRows() const {
	return this->row;
}

int Matrix::getColstride() const {
	return this->colstride;
}

int Matrix::getRowstride() const {
	return this->rowstride;
}

int Matrix::getLen() const {
	return this->len;
}

void Matrix::clear() {
	delete[] this->memory;
	this->memory = new float[len] { 0 };
}

Matrix* Matrix::transpose() {
	if (transposed == nullptr) transposed = new Matrix(memory, len, row, col, rowstride, colstride);
	return transposed;
}

void Matrix::write(const float* buf, int off, int len) {
	for (int i = 0; i < len; i++)
		memory[off + i] = buf[i];
}

float* Matrix::getMemory() const {
	return memory;
}

int Matrix::index(const int r, const int c) const {
	return r * rowstride + c * colstride;
}

void Matrix::mad(const Matrix* a, const Matrix* b, const Matrix* c, const Matrix* d, Matrix* o, bool reset)
{
	if (a->col != b->row)
		throw "Size mismatch: a col";

	if (a->row != o->row)
		throw "Size mismatch: a row";
	if (b->col != o->col)
		throw "Size mismatch: b";

	if (c != nullptr && c->row != b->col)
		throw "Size mismatch: c";

	if (d != nullptr && d->row != a->row)
		throw "Size mismatch: d";

	if (a->row == 1 && a->col == 1 && b->row == 1 && b->col == 1)
	{
		if (reset)
			o->memory[0] = a->memory[0] * b->memory[0] + (c == nullptr ? 0 : c->memory[0]) + (d == nullptr ? 0 : d->memory[0]);
		else
			o->memory[0] += a->memory[0] * b->memory[0] + (c == nullptr ? 0 : c->memory[0]) + (d == nullptr ? 0 : d->memory[0]);
	}
	else if (a->row == 1)
	{
		auto func = [a, b, c, d, o, reset](int j)
		{
			float acc = 0;

			float* a_p = a->memory;
			float* a_pi = a_p;
			for (int i = 0; i < b->row; i++)
				acc += b->memory[b->index(i, j)] * *(a_pi++);

			if (reset)
				o->memory[j] = acc + (c == nullptr ? 0 : c->memory[c->index(j, 0)]) + (d == nullptr ? 0 : d->memory[0]);
			else
				o->memory[j] += acc + (c == nullptr ? 0 : c->memory[c->index(j, 0)]) + (d == nullptr ? 0 : d->memory[0]);
		};
		ThreadPool::parallel_for(0, b->col, func);
	}
	else
	{
		auto func = [a, b, c, d, o, reset](int i)
		{
			for (int j = 0; j < b->col; j++)
			{
				float acc = 0;
				for (int k = 0; k < a->col; k++)
				{
					//TODO: Check if either one is continuously indexed, if so, use a version that uses and increments pointers directly
					acc += a->memory[a->index(i, k)] * b->memory[b->index(k, j)];
				}

				if (reset)
					o->memory[o->index(i, j)] = acc + (c == nullptr ? 0 : c->memory[c->index(j, 0)]) + (d == nullptr ? 0 : d->memory[d->index(i, 0)]);
				else
					o->memory[o->index(i, j)] += acc + (c == nullptr ? 0 : c->memory[c->index(j, 0)]) + (d == nullptr ? 0 : d->memory[d->index(i, 0)]);
			}
		};
		ThreadPool::parallel_for(0, a->row, func);
	}
}

void Matrix::fmop(const Matrix* a, const float a_rate, const Matrix* b, const float b_rate, Matrix* c) {
	if (a != nullptr && a->col != c->col)
		throw "Size mismatch: a col";
	if (a != nullptr && a->row != c->row)
		throw "Size mismatch: a row";

	if (b != nullptr && b->col != c->col)
		throw "Size mismatch: b col";
	if (b != nullptr && b->row != c->row)
		throw "Size mismatch: b row";

	if (c->colstride == 1 && ((a != nullptr && a->colstride == 1) | (a == nullptr)) && ((b != nullptr && b->colstride == 1) | (b == nullptr)))
	{
		float* a_p = a == nullptr ? nullptr : a->memory;
		float* b_p = b == nullptr ? nullptr : b->memory;
		float* c_p = c->memory;

		for (int i = 0; i < c->len; i++, a_p++, b_p++, c_p++)
			* c_p = (a == nullptr ? 0 : *a_p) * a_rate + (b == nullptr ? 0 : *b_p) * b_rate;
	}
	else {
		auto func = [a, a_rate, b, b_rate, &c](int i)
		{
			for (int j = 0; j < c->col; j++)
			{
				c->memory[c->index(i, j)] = (a == nullptr ? 0 : a->memory[a->index(i, j)]) * a_rate + (b == nullptr ? 0 : b->memory[b->index(i, j)]) * b_rate;
			}
		};
		ThreadPool::parallel_for(0, c->row, func);
	}
}

void Matrix::hadamardactiv(const Matrix* a, const Matrix* b, Matrix* c, ActivationFunc* activ)
{
	if (a->col != c->col)
		throw "Size mismatch: a col";

	if (a->row != c->row)
		throw "Size mismatch: a row";

	if (b != nullptr && b->col != c->col)
		throw "Size mismatch: b col";

	if (b != nullptr && b->row != c->row)
		throw "Size mismatch: b row";

	auto func = [&a, b, &activ, &c](int i)
	{
		c->memory[i] = (b == nullptr ? 1 : b->memory[i]) * activ->CPUFunction(a->memory, i, a->len);
	};
	ThreadPool::parallel_for(0, c->len, func);
}

void Matrix::convolve(const Matrix* input, const bool rotInput, const int inputOff, const int inputSz, const int paddingSz, const int dilation, const float strideLen, const Matrix* filter, const bool rotFilter, const int filterOff, const int filterSz, Matrix* output, const bool rotOutput, const int outputOff, const int outputSz, const bool zero, const Matrix* bias, const int bias_off) {
	float* filter_m = &filter->memory[filterOff];
	float* input_m = &input->memory[inputOff];
	float* output_m = &output->memory[outputOff];

	if (filterSz < outputSz)
	{
		int f_sz = filterSz * filterSz - 1;
		for (int c = 0; c < filterSz * filterSz; ++c)
		{
			int x0 = c % filterSz;
			int y0 = c / filterSz;
			float filter_val = filter_m[(rotFilter ? c : f_sz - c)];

			for (int y = 0; y < outputSz; y++)
			{
				int i_y = (int)(y * strideLen + y0 * dilation - paddingSz);
				if (rotInput) i_y = inputSz - 1 - i_y;


				/*if (zero)
					for (int x = 0; x < outputSz; x++)
					{
						if (rotOutput) output_m[(outputSz - 1 - y) * outputSz + (outputSz - 1 - x)] = 0;
						else output_m[y * outputSz + x] = 0;
					}*/

				if (bias != nullptr)
				{
					float b_val = bias->memory[bias_off];
					for (int x = 0; x < outputSz; x++)
					{
						if (zero)
						{
							if (rotOutput) output_m[(outputSz - 1 - y) * outputSz + (outputSz - 1 - x)] = b_val;
							else output_m[y * outputSz + x] = b_val;
						}
						else
						{
							if (rotOutput) output_m[(outputSz - 1 - y) * outputSz + (outputSz - 1 - x)] += b_val;
							else output_m[y * outputSz + x] += b_val;
						}
					}
				}


				if (i_y >= 0 && i_y < inputSz)
					for (int x = 0; x < outputSz; x++)
					{
						int i_x = (int)(x * strideLen + x0 * dilation - paddingSz);
						if (rotInput) i_x = inputSz - 1 - i_x;

						if (i_x >= 0 && i_x < inputSz)
						{
							float output_val = filter_val * input_m[i_y * inputSz + i_x];

							if (zero)
							{
								if (rotOutput) output_m[(outputSz - 1 - y) * outputSz + (outputSz - 1 - x)] = output_val;
								else output_m[y * outputSz + x] = output_val;
							}
							else
							{
								if (rotOutput) output_m[(outputSz - 1 - y) * outputSz + (outputSz - 1 - x)] += output_val;
								else output_m[y * outputSz + x] += output_val;
							}
						}
					}
			}
		}
	}
	else
	{
		int o_sz = outputSz * outputSz - 1;
		for (int c = 0; c < outputSz * outputSz; ++c)
		{
			int x = c % outputSz;
			int y = c / outputSz;
			float output_val = 0;

			if (bias != nullptr)
				output_val += bias->memory[bias_off];

			for (int y0 = 0; y0 < filterSz; y0++)
			{
				int i_y = (int)(y * strideLen + y0 * dilation - paddingSz);
				if (rotInput) i_y = inputSz - 1 - i_y;

				if (i_y >= 0 && i_y < inputSz)
					for (int x0 = 0; x0 < filterSz; x0++)
					{
						int i_x = (int)(x * strideLen + x0 * dilation - paddingSz);
						if (rotInput) i_x = inputSz - 1 - i_x;

						float filter_val = filter_m[(filterSz - 1 - y0) * filterSz + (filterSz - 1 - x0)];
						if (rotFilter) filter_val = filter_m[y0 * filterSz + x0];

						if (i_x >= 0 && i_x < inputSz)
							output_val += filter_val * input_m[i_y * inputSz + i_x];
					}
			}

			if (zero) output_m[(rotOutput ? o_sz - c : c)] = output_val;
			else output_m[(rotOutput ? o_sz - c : c)] += output_val;
		}
	}
}