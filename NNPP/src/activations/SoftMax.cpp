#include "activations/SoftMax.h"

using namespace NNPP;

float SoftMax::Activation::CPUFunction(float* vec, int i, int len) const {
	float num = expf(vec[i]);
	float denom = 0;

	if (vec == p->prev_vec)
		denom = p->prev_denom;
	else {
		for (int j = 0; j < len; j++)
			denom += expf(vec[j]);
		p->prev_denom = denom;
		p->prev_vec = vec;
	}

	return num / denom;
}

float SoftMax::Derivative::CPUFunction(float* vec, int i, int len) const {
	float num = expf(vec[i]);
	float denom = 0;

	throw "Unimplemented";

	if (vec == p->prev_vec)
		denom = p->prev_denom;
	else {
		for (int j = 0; j < len; j++)
			denom += expf(vec[j]);
		p->prev_denom = denom;
		p->prev_vec = vec;
	}

	return (denom - num) * num / (denom * denom);
}