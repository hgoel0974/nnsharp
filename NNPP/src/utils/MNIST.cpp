#include "utils/MNIST.h"
#include <stdio.h>
#include <stdint.h>
#include <iostream>

using namespace NNPP;

MNIST::MNIST() : img_collection(), lbl_collection() {}

typedef struct {
	uint32_t magic;	//2051
	uint32_t img_cnt;
	uint32_t rows;
	uint32_t cols;
	uint8_t imgs[0];
} mnist_img_t;

typedef struct {
	uint32_t magic;	//2049
	uint32_t img_cnt;
	uint8_t imgs[0];
} mnist_lbl_t;

#define BE(x) (((x & 0xff) << 24) | ((x & 0xff00) << 8) | ((x & 0xff0000) >> 8) | ((x & 0xff000000) >> 24))

int MNIST::Read(const char* imgs, const char* labels) {
	FILE* fd = fopen(imgs, "rb");
	if (fd) {
		fseek(fd, 0, SEEK_END);
		int len = ftell(fd);
		fseek(fd, 0, SEEK_SET);
		mnist_img_t* img_data = (mnist_img_t*)new uint8_t[len];
		fread(img_data, 1, len, fd);
		fclose(fd);

		img_data->magic = BE(img_data->magic);
		img_data->img_cnt = BE(img_data->img_cnt);
		img_data->rows = BE(img_data->rows);
		img_data->cols = BE(img_data->cols);

		this->count = img_data->img_cnt;

		int base_off = 0;
		if (img_data->magic == 2051)
			for (int i = 0; i < img_data->img_cnt; i++) {
				Matrix* m = new Matrix(img_data->cols, img_data->rows, false);
				for (int j = 0; j < img_data->cols * img_data->rows; j++)
					m->getMemory()[j] = img_data->imgs[base_off++] / 255.0f;

				img_collection.push_back(m);
			}

		delete[] img_data;
	}
	else
		return -1;

	fd = fopen(labels, "rb");
	if (fd) {
		fseek(fd, 0, SEEK_END);
		int len = ftell(fd);
		fseek(fd, 0, SEEK_SET);
		mnist_lbl_t* img_data = (mnist_lbl_t*)new uint8_t[len];
		fread(img_data, 1, len, fd);
		fclose(fd);

		img_data->magic = BE(img_data->magic);
		img_data->img_cnt = BE(img_data->img_cnt);

		if (img_data->img_cnt != count)
			std::cout << "MNIST Warning: Label data size does not match Image data size." << std::endl;

		int base_off = 0;
		if (img_data->magic == 2049)
			for (int i = 0; i < img_data->img_cnt; i++) {
				Matrix* m = new Matrix(1, 10, true);
				m->getMemory()[img_data->imgs[i]] = 1;

				lbl_collection.push_back(m);
			}

		delete[] img_data;
	}
	else
		return -2;
}

Matrix* MNIST::GetImage(int idx) {
	if (idx < count)
		return img_collection.at(idx);
	return nullptr;
}

Matrix* MNIST::GetLabel(int idx) {
	if (idx < count)
		return lbl_collection.at(idx);
	return nullptr;
}

int MNIST::GetCount() {
	return count;
}