#include "layers/ActivationLayer.h"

using namespace NNPP;

ActivationLayer::ActivationLayer(IActivation* func) {
	activ = func;
}

ActivationLayer::~ActivationLayer() {
	delete activ;
}

Matrix** ActivationLayer::forward(Matrix** input)
{
	PrevInput = input[0];
	//Run activation function
	Matrix::hadamardactiv(input[0], nullptr, Activation, activ->getActivation());
	return &Activation;
}

Matrix** ActivationLayer::propagate(Matrix** input)
{
	Matrix::hadamardactiv(PrevInput, input[0], DeltaActivation, activ->getDerivActivation());
	return &DeltaActivation;
}

Matrix** ActivationLayer::getLastDelta()
{
	return &DeltaActivation;
}

void ActivationLayer::layerError(Matrix** prevDelta)
{
}

void ActivationLayer::learn(IOptimizer& opt)
{
}

void ActivationLayer::resetLayerError()
{
}

int ActivationLayer::getOutputSz() const
{
	return inputSide;
}

int ActivationLayer::getOutputDepth() const
{
	return inputDepth;
}

void ActivationLayer::setInputSz(int input_side, int input_depth)
{
	inputDepth = input_depth;
	inputSide = input_side;
	inputSz = input_side * input_side * input_depth;

	Activation = new Matrix(inputSz, 1, false);
	DeltaActivation = new Matrix(inputSz, 1, false);
}
