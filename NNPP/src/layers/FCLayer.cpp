#include "layers/FCLayer.h"

using namespace NNPP;

FCLayer::FCLayer(int k, int output_dpth) {
	this->k = k;
	this->output_dpth = output_dpth;
	this->layerReset = false;
	this->input_dpth = 0;
	this->input_sz = 0;
}

FCLayer::~FCLayer() {
	delete BiasDelta;
	delete PrevInput;

	delete Weights;
	delete Biases;
	delete ResultMemory;
	delete CurDeltaMemory;
	delete WeightDelta;
}

Matrix** FCLayer::forward(Matrix** input) {
	PrevInput = input[0];
	Matrix::mad(input[0]->transpose(), Weights, Biases, nullptr, ResultMemory->transpose(), true);
	return &ResultMemory;
}

Matrix** FCLayer::propagate(Matrix** prev_delta) {
	//Compute the error to propagate to the following layer
	Matrix::mad(Weights, prev_delta[0], nullptr, nullptr, CurDeltaMemory, true);
	return &CurDeltaMemory;
}

Matrix** FCLayer::getLastDelta() {
	return &CurDeltaMemory;
}

void FCLayer::layerError(Matrix** prev_delta) {
	//Compute the current weights using prev_delta as the error
	Matrix::mad(PrevInput, prev_delta[0]->transpose(), nullptr, nullptr, WeightDelta, layerReset);
	Matrix::fmop(prev_delta[0], 1.0f, BiasDelta, layerReset ? 0.0f : 1.0f, BiasDelta);

	layerReset = false;
}

void FCLayer::learn(IOptimizer& opt) {
	if(optimizer_idx == -1) optimizer_idx = opt.RegisterLayer(this, 1, input_sz * input_sz * input_dpth, k * k * output_dpth, 1, k * k * output_dpth);
	opt.OptimizeWeights(this, optimizer_idx, 0, Weights, WeightDelta);
	opt.OptimizeBiases(this, optimizer_idx, 0, Biases, BiasDelta);
}

void FCLayer::resetLayerError() {
	layerReset = true;
}

int FCLayer::getOutputSz() const {
	return k;
}

int FCLayer::getOutputDepth() const {
	return output_dpth;
}

void FCLayer::setInputSz(int sz, int dpth) {
	input_sz = sz;
	input_dpth = dpth;

	if (Weights == nullptr) Weights = new Matrix(sz * sz * input_dpth, k * k * output_dpth, false);
	if (Biases == nullptr) Biases = new Matrix(k * k * output_dpth, 1, false);

	BiasDelta = new Matrix(k * k * output_dpth, 1, false);
	WeightDelta = new Matrix(sz * sz * input_dpth, k * k * output_dpth, false);
	ResultMemory = new Matrix(k * k * output_dpth, 1, false);
	CurDeltaMemory = new Matrix(sz * sz * input_dpth, 1, false);
}

void FCLayer::setWeights(IWeightInitializer* init) {
	float* m_ws = new float[Weights->getRows()];
	float* b_ws = new float[Biases->getRows()];

	for (int j = 0; j < Weights->getColumns() * Weights->getRows(); j++)
	{
		Weights->getMemory()[j] = (float)init->getWeight(0, 0, 0, Weights->getColumns(), Weights->getRows(), 0); //(i + j * Weights.Height + 1) / (Weights.Width * Weights.Height + 1); //
	}

	for (int i = 0; i < Biases->getRows(); i++)
	{
		b_ws[i] = init->getBias(0, 0);
	}

	Biases->write(b_ws, 0, Biases->getRows());
}