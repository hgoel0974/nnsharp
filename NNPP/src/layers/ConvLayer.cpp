#include "layers/ConvLayer.h"
#include "ThreadPool.h"

using namespace NNPP;

ConvLayer::ConvLayer(int filter_side, int filter_cnt, int padding, float stride, int dilation)
{
	filterSz = filter_side;
	filterCnt = filter_cnt;
	paddingSz = padding;
	strideLen = stride;
	this->dilation = dilation;
	WeightErrorsReset = false;
}

ConvLayer::~ConvLayer() {
	delete BiasError;
	delete Output;
	delete PrevInput;
	delete BackwardDelta;
	delete Bias;

	if (Weights != nullptr)
		for (int i = 0; i < filterCnt; i++)
		{
			if (Weights[i] != nullptr)
				for (int j = 0; j < inputDepth; j++)
				{
					delete Weights[i][j];
				}
			delete[] Weights[i];
		}
	delete[] Weights;

	if (WeightErrors != nullptr)
		for (int i = 0; i < filterCnt; i++)
		{
			if (WeightErrors[i] != nullptr)
				for (int j = 0; j < inputDepth; j++)
				{
					delete WeightErrors[i][j];
				}
			delete[] WeightErrors[i];
		}
	delete[] WeightErrors;
}

Matrix** ConvLayer::forward(Matrix** input)
{
	PrevInput = input[0];

	Output->clear();
	auto func = [this, input](int i)
	{
		//Foreach Input Layers
		for (int j = 0; j < inputDepth; j++)
		{
			Matrix::convolve(input[0], false, j * inputSz * inputSz, inputSz, paddingSz, dilation, strideLen, Weights[i][j], false, 0, filterSz, Output, false, i * outputSz * outputSz, outputSz, false, (j == 0 ? Bias : nullptr), i);
		}
	};
	ThreadPool::parallel_for(0, filterCnt, func);

	return &Output;
}

Matrix** ConvLayer::propagate(Matrix** prev_delta)
{
	BackwardDelta->clear();
	int padd = (int)(((inputSz - 1) * strideLen - outputSz + filterSz * dilation) / 2);

	auto func = [this, padd, prev_delta](int j)
	{
		for (int i = 0; i < filterCnt; i++)
		{
			Matrix::convolve(prev_delta[0], false, i * outputSz * outputSz, outputSz, padd, dilation, strideLen, Weights[i][j], true, 0, filterSz, BackwardDelta, false, j * inputSz * inputSz, inputSz, false);
		}
	};
	ThreadPool::parallel_for(0, inputDepth, func);

	return &BackwardDelta;
}

Matrix** ConvLayer::getLastDelta()
{
	return &BackwardDelta;
}

void ConvLayer::layerError(Matrix** prev_delta)
{
	auto func = [this, prev_delta](int i)
	{
		float acc = 0;
		auto prev_delta_memory = prev_delta[0]->getMemory();
		for (int x = 0; x < outputSz * outputSz; x++)
			acc += prev_delta_memory[prev_delta[0]->index(i * outputSz * outputSz + x, 0)];

		if (WeightErrorsReset)
			BiasError->getMemory()[BiasError->index(i, 0)] = 0;

		BiasError->getMemory()[BiasError->index(i, 0)] += acc;

		for (int j = 0; j < inputDepth; j++)
		{
			int padd = (int)(((filterSz - 1) * strideLen - inputSz + outputSz * dilation) / 2);
			Matrix::convolve(PrevInput, false, j * inputSz * inputSz, inputSz, padd, dilation, strideLen, prev_delta[0], true, i * outputSz * outputSz, outputSz, WeightErrors[i][j], true, 0, filterSz, WeightErrorsReset);
		}
	};
	ThreadPool::parallel_for(0, filterCnt, func);
	WeightErrorsReset = false;
}

void ConvLayer::learn(IOptimizer& opt)
{
	if(optimizer_idx == -1) optimizer_idx = opt.RegisterLayer(this, filterCnt * inputDepth, filterSz, filterSz, 1, filterCnt);

	//for (int i = 0; i < filterCnt; i++)
	auto func = [this, &opt](int i)
	{
		for (int j = 0; j < inputDepth; j++)
		{
			opt.OptimizeWeights(this, optimizer_idx, i * inputDepth + j, Weights[i][j], WeightErrors[i][j]);
		}
	};
	ThreadPool::parallel_for(0, filterCnt, func);

	opt.OptimizeBiases(this, optimizer_idx, 0, Bias, BiasError);
}

void ConvLayer::resetLayerError()
{
	WeightErrorsReset = true;
}

int ConvLayer::getOutputSz() const
{
	return outputSz;
}

int ConvLayer::getOutputDepth() const
{
	return filterCnt;
}

void ConvLayer::setInputSz(int input_side, int input_depth)
{
	inputDepth = input_depth;
	inputSz = input_side;
	outputSz = (int)((inputSz - filterSz * dilation + 2 * paddingSz) / strideLen + 1);
	//o = (i - f * d + 2 * p) / s + 1
	//Allocate memory for the filters
	//Weights = new Matrix(filterSz * filterSz * filterCnt, inputDepth, MemoryFlags.ReadWrite, false);
	//Each filter is filterSz x filterSz x inputDepth
	//The output for each point of the filter is the sum of the convolution of each individual filter
	if (Weights == nullptr)
	{
		Weights = new Matrix * *[filterCnt];//26 - 22 = (2p)

		for (int i = 0; i < filterCnt; i++)
		{
			Weights[i] = new Matrix * [inputDepth];
			for (int j = 0; j < inputDepth; j++)
			{
				Weights[i][j] = new Matrix(filterSz, filterSz, false);
			}
		}
		Bias = new Matrix(filterCnt, 1, false);
	}

	WeightErrors = new Matrix * *[filterCnt];

	for (int i = 0; i < filterCnt; i++)
	{
		WeightErrors[i] = new Matrix * [inputDepth];
		for (int j = 0; j < inputDepth; j++)
		{
			WeightErrors[i][j] = new Matrix(filterSz, filterSz, false);
		}
	}
	Bias = new Matrix(filterCnt, 1, false);

	//Need intermediate storage for each filter
	Output = new Matrix(outputSz * outputSz * filterCnt, 1, false);
	BiasError = new Matrix(filterCnt, 1, false);

	BackwardDelta = new Matrix(inputSz * inputSz * inputDepth, 1, false);
}

void ConvLayer::setWeights(IWeightInitializer* init) {
	int f_sz2 = filterSz * filterSz;

	float* rng = new float[f_sz2];
	float* rng_b = new float[filterCnt];
	for (int i = 0; i < filterCnt; i++)
	{
		for (int j = 0; j < inputDepth; j++)
		{
			for (int x = 0; x < f_sz2; x++)
			{
				rng[x] = init->getWeight(0, 0, 0, filterSz, filterSz, 0) / filterCnt;
			}

			Weights[i][j]->write(rng, 0, f_sz2);
		}
		rng_b[i] = init->getBias(0, 0);
	}
	Bias->write(rng_b, 0, filterCnt);
}