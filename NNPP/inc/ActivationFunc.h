#pragma once
namespace NNPP {
	class ActivationFunc
	{
	public:
		virtual float CPUFunction(float* vec, int i, int len) const = 0;
	};
}

