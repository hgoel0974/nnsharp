#pragma once
#include "ILayer.h"
#include "ActivationFunc.h"
#include "IActivation.h"
#include "Matrix.h"

namespace NNPP {
	class ActivationLayer : public ILayer {
	private:
		IActivation* activ = nullptr;
		int inputSide = 0, inputDepth = 0, inputSz = 0;


	public:
		Matrix* Activation = nullptr;
		Matrix* PrevInput = nullptr;
		Matrix* DeltaActivation = nullptr;
		
		ActivationLayer(IActivation* func);
		~ActivationLayer();

		// Inherited via ILayer
		Matrix** forward(Matrix** input) override;
		Matrix** propagate(Matrix** input) override;
		Matrix** getLastDelta() override;
		void layerError(Matrix** prevDelta) override;
		void learn(IOptimizer& opt) override;
		void resetLayerError() override;
		int getOutputSz() const override;
		int getOutputDepth() const override;
		void setInputSz(int inputSide, int inputDepth) override;
	};
}