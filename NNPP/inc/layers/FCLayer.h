#pragma once
#include "ILayer.h"
#include "IWeightInitializable.h"
#include "IWeightInitializer.h"
#include "IOptimizer.h"
#include "Matrix.h"

namespace NNPP {
	class FCLayer :
		public ILayer, public IWeightInitializable
	{
	private:
		int k;
		int output_dpth;
		int input_sz, input_dpth;
		bool layerReset;
		Matrix *BiasDelta = nullptr;
		Matrix *PrevInput = nullptr;

		int optimizer_idx = -1;

	public:
		Matrix *Weights = nullptr;
		Matrix *Biases = nullptr;
		Matrix *ResultMemory = nullptr;
		Matrix *CurDeltaMemory = nullptr;
		Matrix *WeightDelta = nullptr;

		FCLayer(int k, int output_dpth);
		~FCLayer();

		Matrix** forward(Matrix** input) override;
		Matrix** propagate(Matrix** input) override;
		Matrix** getLastDelta() override;

		void layerError(Matrix** prevDelta) override;
		void learn(IOptimizer& opt) override;
		void resetLayerError() override;

		int getOutputSz() const override;
		int getOutputDepth() const override;
		void setInputSz(int inputSide, int inputDepth) override;

		void setWeights(IWeightInitializer* init) override;
	};
}

