#pragma once
#include "ILayer.h"
#include "IWeightInitializable.h"
#include "IWeightInitializer.h"
#include "IOptimizer.h"
#include "Matrix.h"

namespace NNPP {
	class ConvLayer : public ILayer, public IWeightInitializable
	{
	private:
		int inputSz = 0;
		int outputSz = 0;
		int inputDepth = 0;
		int filterSz = 0 /*F*/;
		int paddingSz = 0 /*P*/;
		int filterCnt = 0 /*K*/;
		float strideLen = 0 /*S*/;
		int dilation = 0 /*D*/;
		Matrix* BiasError = nullptr;
		Matrix* Output = nullptr;
		Matrix* PrevInput = nullptr;
		Matrix* BackwardDelta = nullptr;
		bool WeightErrorsReset;

		int optimizer_idx = -1;

	public:
		Matrix*** WeightErrors = nullptr;
		Matrix*** Weights = nullptr;
		Matrix* Bias = nullptr;

		ConvLayer(int filter_side, int filter_cnt, int padding = 0, float stride = 1, int dilation = 1);
		~ConvLayer();

		// Inherited via ILayer
		Matrix** forward(Matrix** input) override;
		Matrix** propagate(Matrix** input) override;
		Matrix** getLastDelta() override;
		void layerError(Matrix** prevDelta) override;
		void learn(IOptimizer& opt) override;
		void resetLayerError() override;
		int getOutputSz() const override;
		int getOutputDepth() const override;
		void setInputSz(int inputSide, int inputDepth) override;

		void setWeights(IWeightInitializer* init) override;
	};
}

