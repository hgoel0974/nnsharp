#pragma once
#include "Matrix.h"

namespace NNPP {
	class ILossFunction {
		virtual void Loss(Matrix* output, Matrix* expectedOutput, Matrix* result, float regularizationParam) = 0;
		virtual void LossDeriv(Matrix* output, Matrix* expectedOutput, Matrix* result, float regularizationParam) = 0;
	};
}