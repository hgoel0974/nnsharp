#pragma once
#include "ILayer.h"
#include "Matrix.h"

namespace NNPP {
	class IOptimizer {
	public:
		virtual int RegisterLayer(ILayer* layer, int w_cnt, int ww_len, int wh_len, int b_cnt, int b_len) = 0;
		virtual void OptimizeWeights(ILayer* layer, int l_idx, int idx, Matrix* w, Matrix* nabla_w) = 0;
		virtual void OptimizeBiases(ILayer* layer, int l_idx, int idx, Matrix* w, Matrix* nabla_w) = 0;
		virtual void Update(float curError) = 0;
	};
}