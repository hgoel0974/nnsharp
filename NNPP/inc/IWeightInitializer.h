#pragma once
namespace NNPP {
	class IWeightInitializer {
	public:
		virtual float getBias(int x, int z) = 0;
		virtual float getWeight(int x, int y, int z, int w, int h, int d) = 0;
	};
}