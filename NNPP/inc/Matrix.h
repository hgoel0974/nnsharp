#pragma once
#include "ActivationFunc.h"

namespace NNPP {
	class Matrix
	{
	private:
		float* memory = nullptr;
		int col = 0, row = 0, len = 0;
		int colstride = 0, rowstride = 0;
		Matrix* transposed = nullptr;

		Matrix(float* mem, int len, int col, int row, int colstride, int rowstride);

	public:
		Matrix(int col, int row, bool zero);
		Matrix();
		~Matrix();

		void init(int col, int row, bool zero);

		int getColumns() const;
		int getRows() const;
		int getColstride() const;
		int getRowstride() const;
		int getLen() const;

		float* getMemory() const;

		int index(const int r, const int c) const;

		void clear();
		Matrix* transpose();
		void write(const float* buf, int off, int len);

		static void mad(const Matrix* a, const Matrix* b, const Matrix* c, const Matrix* d, Matrix* o, bool reset);
		static void fmop(const Matrix* a, const float a_rate, const Matrix* b, const float b_rate, Matrix* o);
		static void hadamardactiv(const Matrix* a, const Matrix* b, Matrix* c, ActivationFunc* activ);
		static void convolve(const Matrix* input, const bool rotInput, const int inputOff, const int inputSz, const int paddingSz, const int dilation, const float strideLen, const Matrix* filter, const bool rotFilter, const int filterOff, const int filterSz, Matrix* output, const bool rotOutput, const int outputOff, const int outputSz, const bool zero, const Matrix* bias = nullptr, const int bias_off = 0);

		//TODO: Serialization

	};
}