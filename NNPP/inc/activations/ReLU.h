#pragma once
#include "ActivationFunc.h"

namespace NNPP {
	class ReLU : public IActivation {
	public:
		class Activation : ActivationFunc {
		private:
			friend class ReLU;
			float alpha;
			Activation(float a) {
				alpha = a;
			}
		public:
			// Inherited via ActivationFunc
			float CPUFunction(float* vec, int i, int len) const override {
				if (vec[i] < 0) return alpha * vec[i];
				return vec[i];
			}
		};

		class Derivative : ActivationFunc {
		private:
			friend class ReLU;
			float alpha;
			Derivative(float a) {
				alpha = a;
			}
		public:
			// Inherited via ActivationFunc
			float CPUFunction(float* vec, int i, int len) const override {
				if (vec[i] < 0) return alpha;
				return 1.0f;
			}
		};

		ReLU(float alpha = 0.0f) : activ(alpha), deriv(alpha) { }
		ActivationFunc* getActivation() override {
			return &activ;
		}
		ActivationFunc* getDerivActivation() override {
			return &deriv;
		}
	private:
		Activation activ;
		Derivative deriv;
	};
}