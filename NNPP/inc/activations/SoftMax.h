#pragma once
#include "ActivationFunc.h"
#include "IActivation.h"
#include <math.h>

namespace NNPP {
	class SoftMax : public IActivation {
	public:
		class Activation : ActivationFunc {
		private:
			friend class SoftMax;
			SoftMax* p;
			Activation(SoftMax* parent) { p = parent; }

		public:
			// Inherited via ActivationFunc
			float CPUFunction(float* vec, int i, int len) const override;
		};

		class Derivative : ActivationFunc {
		private:
			friend class SoftMax;
			SoftMax* p;
			Derivative(SoftMax* parent) { p = parent; };

		public:
			// Inherited via ActivationFunc
			float CPUFunction(float* vec, int i, int len) const override;
		};

		SoftMax() : activ(this), deriv(this) { }
		ActivationFunc* getActivation() override {
			return &activ;
		}
		ActivationFunc* getDerivActivation() override {
			return &deriv;
		}

	private:
		float* prev_vec = nullptr;
		float prev_denom = 0;

		Activation activ;
		Derivative deriv;
	};
}