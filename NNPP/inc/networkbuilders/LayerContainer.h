#pragma once
#include "ILayer.h"
#include "ILossFunction.h"
#include "IWeightInitializer.h"
#include "Matrix.h"

#include <vector>

namespace NNPP {
	class LayerContainer
	{
	private:
		std::vector<ILayer*> layers;
		ILossFunction* loss_func;
		IWeightInitializer* weight_init;
		int input_side, input_depth, output_side, output_depth;
	public:
		LayerContainer(int input_side, int input_depth);
		LayerContainer* Append(ILayer* layer);
		LayerContainer* SetLoss(ILossFunction* lossfunc);
		LayerContainer* SetWeightInitializer(IWeightInitializer* initializer);

		Matrix** Forward(Matrix** input);
		Matrix** Propagate(Matrix** prev_delta);
		void ComputeErrors(Matrix** prev_delta);
		void Learn(IOptimizer& opt);

		void Build();
	};
}

