#pragma once
#include "Matrix.h"

namespace NNPP {
	class IOptimizer;
	class ILayer
	{
	public:
		virtual Matrix** forward(Matrix** input) = 0;
		virtual Matrix** propagate(Matrix** input) = 0;
		virtual Matrix** getLastDelta() = 0;

		virtual void layerError(Matrix ** prevDelta) = 0;
		virtual void learn(IOptimizer &opt) = 0;
		virtual void resetLayerError() = 0;

		virtual int getOutputSz() const = 0;
		virtual int getOutputDepth() const = 0;
		virtual void setInputSz(int inputSide, int inputDepth) = 0;
	};
}