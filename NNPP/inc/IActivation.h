#pragma once
#include "ActivationFunc.h"

namespace NNPP {
	class IActivation {
	public:
		virtual ActivationFunc* getActivation() = 0;
		virtual ActivationFunc* getDerivActivation() = 0;
	};
}