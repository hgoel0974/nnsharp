#pragma once
#include "IOptimizer.h"

namespace NNPP {
	class Adam :
		public IOptimizer
	{
	private:
		struct AdamParams
		{
		public:
			Matrix* m_w = nullptr;
			Matrix* v_w = nullptr;

			Matrix* m_b = nullptr;
			Matrix* v_b = nullptr;
		};

		float learning_rate;
		float beta_1;
		float beta_2;
		float epsilon;
		AdamParams *layers;

		float L1Val;
		float L2Val;
		int Net;

		int layer_cnt = 0;
		int idx = 0;

	public:
		Adam(int layer_cnt, float learning_rate = 0.001f, float epsilon = 1e-6f, float beta_1 = 0.9f, float beta_2 = 0.999f);
		~Adam();

		// Inherited via IOptimizer
		int RegisterLayer(ILayer* layer, int w_cnt, int ww_len, int wh_len, int b_cnt, int b_len) override;
		void OptimizeWeights(ILayer* layer, int l_idx, int idx, Matrix* w, Matrix* nabla_w) override;
		void OptimizeBiases(ILayer* layer, int l_idx, int idx, Matrix* w, Matrix* nabla_w) override;
		void Update(float curError) override;

		float getL1Val() const { return L1Val; }
		float getL2Val() const { return L2Val; }
		int getNet() const { return Net; }
	};
}

