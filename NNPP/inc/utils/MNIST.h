#pragma once
#include "Matrix.h"

#include <vector>

namespace NNPP {
	class MNIST {
	private:
		std::vector<Matrix*> img_collection;
		std::vector<Matrix*> lbl_collection;
		int count;
	public:
		MNIST();
		int Read(const char* imgs, const char* labels);
		Matrix* GetImage(int idx);
		Matrix* GetLabel(int idx);
		int GetCount();
	};
}