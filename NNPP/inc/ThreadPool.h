#pragma once
#include <thread>

namespace NNPP {
	class ThreadPool
	{
	public:
		template <typename Proc>
		static void parallel_for(int base, int dst, Proc& func) {
			auto thread_cnt = std::thread::hardware_concurrency();
			auto threads = new std::thread * [thread_cnt]();

			int t_idx = 0;
			for (int i = base; i < dst; i++) {
				t_idx = i % thread_cnt;

				if (threads[t_idx] != nullptr) {
					threads[t_idx]->join();
					delete threads[t_idx];
				}

				threads[t_idx] = new std::thread(func, i);
				//threads[t_idx]->
			}

			for (unsigned int i = 0; i < thread_cnt; i++) {
				threads[i]->join();
				delete threads[i];
			}

			delete[] threads;
		}
	};
}

