#pragma once
#include "ILossFunction.h"
#include "ThreadPool.h"
#include "Matrix.h"

namespace NNPP {
	class Quadratic : public ILossFunction {
		// Inherited via ILossFunction
		void Loss(Matrix* output, Matrix* expectedOutput, Matrix* result, float regularizationParam) override {
			auto func = [result, expectedOutput, output, regularizationParam](int i)
			{
				float diff = (expectedOutput->getMemory()[i] - output->getMemory()[i]);
				result->getMemory()[i] += 0.5f * diff * diff / result->getRows() + regularizationParam;
			};
			ThreadPool::parallel_for(0, result->getRows(), func);
		}

		void LossDeriv(Matrix* output, Matrix* expectedOutput, Matrix* result, float regularizationParam) override {
			auto func = [result, expectedOutput, output, regularizationParam](int i)
			{
				float diff = (expectedOutput->getMemory()[i] - output->getMemory()[i]) / result->getRows() + regularizationParam;
				result->getMemory()[i] += diff;
			};
			ThreadPool::parallel_for(0, result->getRows(), func);
		}
	};
}