#pragma once
#include "ILossFunction.h"
#include "ThreadPool.h"
#include "Matrix.h"

namespace NNPP {
	class BinaryCrossEntropy : public ILossFunction {
		// Inherited via ILossFunction
		void Loss(Matrix* output, Matrix* expectedOutput, Matrix* result, float regularizationParam) override {
			//- (z * log(y + eps) + (1-z) * log(1 - y + eps))
						//z = expected out
						//y = actual output
			auto func = [result, expectedOutput, output, regularizationParam](int i)
			{
				result->getMemory()[i] += (-(float)(expectedOutput->getMemory()[i] * logf(output->getMemory()[i] + FLT_EPSILON) + (1 - expectedOutput->getMemory()[i]) * logf(1 - output->getMemory()[i] + FLT_EPSILON)) / result->getRows() + regularizationParam);
			};
			ThreadPool::parallel_for(0, result->getRows(), func);
		}

		void LossDeriv(Matrix* output, Matrix* expectedOutput, Matrix* result, float regularizationParam) override {
			//- (z / (y + eps) - (1 - z) / (1 - y + eps));
			//z = expected out
			//y = actual output
			auto func = [result, expectedOutput, output, regularizationParam](int i)
			{
				float r_0 = ((1.0f - expectedOutput->getMemory()[i]) - output->getMemory()[i]);
				float r = (r_0 == 0) ? 0 : (1.0f / r_0);
				result->getMemory()[i] += r / result->getRows() + regularizationParam;
			};
			ThreadPool::parallel_for(0, result->getRows(), func);
		}
	};
}