#pragma once
#include "IWeightInitializer.h"
namespace NNPP {
	class IWeightInitializable {
	public:
		virtual void setWeights(IWeightInitializer* init) = 0;
	};
}