#pragma once
#include "IWeightInitializer.h"
#include <random>

namespace NNPP {
	class UniformWeightInitializer :
		public IWeightInitializer
	{
	private:
		std::default_random_engine rng;
		std::uniform_real_distribution<float> dist;
		float bias;

	public:
		UniformWeightInitializer(float b, int seed = 5489) : dist(0, 1), rng(seed), bias(b) { };

		// Inherited via IWeightInitializer
		float getBias(int x, int z) override;
		float getWeight(int x, int y, int z, int w, int h, int d) override;
	};
}

